var hotels ={
    "hotels" :[
        {
            "name" : "Hotel Emperador",
            "stars" : 3,
            "price" : 1596,
        },
        {
            "name" : "Petit Palace San Bernardo",
            "stars" : 4,
            "price" : 2145,
        },
        {
            "name" : "Hotel Nuevo Boston",
            "stars" : 2,
            "price" : 861,
        }
    ]
}
exports.findAllHostels = function(req, res) {
    console.log('GET /hotels')
		res.status(200).jsonp(hotels);
};