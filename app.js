var express = require("express"),
    app = express(),
    bodyParser  = require("body-parser"),
    methodOverride = require("method-override");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


var HotelsCtrl = require('./controllers/hotels');

// API routes
var hotels = express.Router();

hotels.route('/hotels')
    .get(HotelsCtrl.findAllHostels)

app.use('/api', hotels);


app.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});